import {
    html,
    css,
    LitElement
} from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

export class CodeWriter extends LitElement {
	static properties = {

	}
	/**
	 * Constructor del componente
	 */
	constructor() {
		super();
	}
	/**
	 * Método llamado por LIT para renderizar el elemento
	 * Debe devolver el html que se va a mostrar en dónde está el elemento
	 */
	render() {
		return html`
		<div>Introduce el código del producto</div>
		<input type="text" id="userQuery">
		<button @click="${this.searchPrice}">
			Consultar
		</button>
		`;
	}
	/**
	 * Buscar el código introducido en el elemento con id = userQuery
	 */
	getCodeValue() {
		/*
		 * es el equivalente a usar document
		 * pero restringido a la raíz de este componente,
		 * Con lo cual no busca en todo el documento y se evitan colisiones
		 * con otros componentes
		 */
		const result = this.renderRoot.querySelector("#userQuery").value;
		return result;		
	}
	/**
	 * Emite el evento searchPrice cada vez que el usaurio presiona enter
	 */
	searchPrice() {
		const code = this.getCodeValue();
		const eventData = {code};
		const myEvent = new CustomEvent('searchPrice', {detail: eventData});
		this.dispatchEvent(myEvent);
	}
}
/** Registramos el componente en el browser */
customElements.define('ceco-code-writer', CodeWriter)